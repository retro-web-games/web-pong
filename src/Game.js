import Core from './Core.js'

import Playground from './Objects/Playground'
import Player from './Objects/Player'
import PlayerAi from './Objects/PlayerAi'
import Ball from './Objects/Ball'

class Game extends Core {
    constructor(container){
        super(container)

        this.objects = {}
        this.objects.playground = new Playground({}, this.container)
        this.objects.ball = new Ball({}, this.container)
        this.objects.player = new Player({ x: 0 }, this.container)
        this.objects.bot = new PlayerAi({ x: this.container.canvas.width - this.objects.player.width, target: this.objects.ball, difficulty: 3 }, this.container)

        this.container.canvas.addEventListener('mousemove', function(event){
            this.objects.player.y = event.clientY-this.objects.player.height/2
        }.bind(this))

        this.run()
    }

    update(){
        let winner = this.objects.ball.bounce(this.objects.player, this.objects.bot)

        if(winner != null){
            winner.score++
            this.reset()
        }

        this.draw()
    }

    reset(){
        this.objects.ball.x = this.container.canvas.width/2
        this.objects.ball.y = this.container.canvas.height/2

        this.objects.ball.velocity.y = 0
    }
}

export default Game