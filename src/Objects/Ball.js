import Moveable from './Moveable.js'

class Ball extends Moveable{
    constructor(data, container){
        super(data, container)

        this.radius = this.container.canvas.height/150
        this.x = this.container.canvas.width/2
        this.y = this.container.canvas.height/2
        this.velocity = {
            x: 4,
            y: 0
        }
        this.center = {
            x: this.x+this.radius/2,
            y: this.y+this.radius/2
        }
        this.velocity_range = this.container.canvas.width/10 + 20
    }

    draw(){
        this.x += this.velocity.x * (this.velocity_range/this.container.fps)
        this.y += this.velocity.y * (this.velocity_range/this.container.fps)
        this.center = {
            x: this.x+this.radius/2,
            y: this.y+this.radius/2
        }

        this.container.context.fillStyle = 'white'
        this.container.context.beginPath();
        this.container.context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
        this.container.context.fill();
    }

    bounce(player_1, player_2){
        if(this.center.y < 0 && this.velocity.y < 0){
            this.velocity.y *= -1
        }
        if(this.center.y > this.container.canvas.height && this.velocity.y > 0){
            this.velocity.y *= -1
        }
        if(this.center.x < 0+player_1.width){
            if(this.y > player_1.y && this.y < player_1.y+player_1.height){
                this.velocity.x *= -1 
                this.velocity.y = (this.y-(player_1.y + player_1.height/2))*(player_1.height/this.container.canvas.height/2) 
            }
            else{
                return player_2
            }
        }
        if(this.center.x > this.container.canvas.width-player_2.width){
            if(this.y > player_2.y && this.y < player_2.y+player_2.height){
                this.velocity.x *= -1 
                this.velocity.y = (this.y-(player_2.y + player_2.height/2))*(player_2.height/this.container.canvas.height/2)
            }
            else{
                return player_1
            }
        }

        return null
    }
}

export default Ball