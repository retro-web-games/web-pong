import Drawable from './Drawable'

class Playground extends Drawable {
    draw(){
        this.container.context.fillStyle = 'black'
        this.container.context.fillRect(0, 0, this.container.canvas.width, this.container.canvas.height)
    }
}

export default Playground