import Drawable from './Drawable'

class Playground extends Drawable {
    constructor(data, container){
        super(data, container)

        this.width = 10
        this.height = this.container.canvas.height/5
        this.y = this.container.canvas.height/2 - this.height/2
        this.score = 0
    }

    draw(){
        this.container.context.fillStyle = 'white'
        this.container.context.fillRect(this.x, this.y, this.width, this.height)
        this.container.context.fillText(this.score, this.container.canvas.width/4+this.x/2, 30)
    }
}

export default Playground