class Drawable{
    constructor(data, container){
        this.container = container

        this.x = data.x
        this.y = data.y
        this.width = data.width
        this.height = data.height
    }

    draw(){
        console.log('draw function - todo')
    }
}

export default Drawable;