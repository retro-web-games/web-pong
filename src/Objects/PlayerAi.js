import Player from './Player'

class PlayerAi extends Player {
    constructor(data, container){
        super(data, container)

        this.target = data.target
        this.difficulty = data.difficulty || 0

        this.offset = this.height/10
    }

    draw(){
        switch(this.difficulty){
            case 0:
                if(this.y-this.offset*2 < this.target.y-(this.height/2)){
                    this.y += this.offset
                }
                if(this.y+this.offset*2 > this.target.y-(this.height/2)){
                    this.y -= this.offset
                }
                break
            case 1:
            if(this.y-this.offset/2 < this.target.y-(this.height/2)){
                    this.y += this.offset
                }
                if(this.y+this.offset/2 > this.target.y-(this.height/2)){
                    this.y -= this.offset
                }
                break
            case 2:
                this.y = this.target.y-(this.height/2)
                break
            case 3:
                if(Math.round(Math.random())){
                    this.y = this.target.y-(this.height/100)
                }
                else{
                    this.y = this.target.y-(this.height)+(this.height/100)
                }
                
                break
        }

        super.draw()
    }
}

export default PlayerAi