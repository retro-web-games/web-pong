import Drawable from './Drawable'

class Moveable extends Drawable {
    constructor(data, container){
        super(data, container)

        this.velocity = data.velocity
    }
}

export default Moveable;