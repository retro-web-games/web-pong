class Core {
    constructor(container, opt){
        this.container = container

        if(opt == null){
            opt = {}
        }
        
        this.defaultOptions = this.defaultOptions || {}
        this.defaultOptions.max_fps = 60
        this.defaultOptions.show_fps = true

        this.options = {}
        for(let prop in this.defaultOptions){
            this.options[prop] = this.options[prop] || opt[prop] || this.defaultOptions[prop]
        }

        this.container.context = this.container.canvas.getContext('2d')
        this.container.context.font = '20px Courier New'
        this.container.fps = 0

        this.now = null
        this.then = Date.now()
        this.interval = 1000/this.options.max_fps
        this.delta = null

        this.fps_counter = {
            refresh: 200,
            before: new Date().getTime(),
            now: null,
            fps: 0
        }
    }

    run(){
        requestAnimationFrame(this.run.bind(this));

        this.now = new Date().getTime();
        this.delta = this.now - this.then;

        if (this.delta > this.interval) {
            // update time stuffs
            this.then = this.now - (this.delta % this.interval);

            // calculate the frames per second
            this.container.fps = Math.round(1000/this.delta)

            this.update()

            if(this.options.show_fps){
                this.fps_counter.now = new Date().getTime()

                if(this.fps_counter.now - this.fps_counter.before > this.fps_counter.refresh){
                    this.fps_counter.before = this.fps_counter.now

                    this.fps_counter.fps = this.container.fps
                }

                this.container.context.fillStyle = 'white'
                this.container.context.fillText(this.fps_counter.fps + ' FPS', this.container.canvas.width-90, this.container.canvas.height-20)
            }
        }
    }

    update(){
        console.log('update function - todo')
    }

    draw(){
        for(let label in this.objects){
            this.objects[label].draw()
        }
    }
}

export default Core